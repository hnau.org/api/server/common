package org.hnau.api.server.common

import org.hnau.api.common.endpoint.Endpoint
import org.hnau.api.common.endpoint.EndpointKey
import org.hnau.api.common.response.ApiResponseReadWriteFactory
import org.hnau.api.common.response.error.ApiError
import org.hnau.api.common.response.error.ApiException
import org.hnau.api.common.response.error.exception
import org.hnau.base.data.bytes.provider.BytesProvider
import org.hnau.base.data.bytes.provider.read
import org.hnau.base.data.bytes.receiver.BytesReceiver
import org.hnau.base.data.maybe.Maybe
import org.hnau.base.data.maybe.error
import org.hnau.base.data.maybe.success
import org.hnau.base.extensions.castOrElse

typealias ApiEndpointHandler<S> = suspend BytesProvider.(S) -> (BytesReceiver.() -> Unit)

data class NamedApiEndpointHandler<S>(
        val name: String,
        val handle: ApiEndpointHandler<S>
)

data class ApiEndpointHandlerRaw<S, I, O>(
        val endpoint: Endpoint<I, O>,
        val readParam: BytesProvider.() -> I,
        val writeResponse: BytesReceiver.(O) -> Unit,
        val handle: suspend (S, I) -> O
) {

    inline fun wrapHandle(
            crossinline onBegin: (S, Endpoint<I, O>, I) -> Unit,
            crossinline onEnd: (S, Maybe<O>) -> Unit
    ) = copy(
            handle = { session, param ->
                onBegin(session, endpoint, param)
                try {
                    handle(session, param)
                            .also { onEnd(session, Maybe.success(it)) }
                } catch (th: Throwable) {
                    onEnd(session, Maybe.error(th))
                    throw th
                }
            }
    )

    fun toApiEndpointHandler() = ApiEndpointHandler(
            endpoint = endpoint,
            readParam = readParam,
            writeResponse = writeResponse,
            handle = handle
    )

}

inline fun <S, I, O> ApiEndpointHandler(
        endpoint: Endpoint<I, O>,
        crossinline readParam: BytesProvider.() -> I,
        crossinline writeResponse: BytesReceiver.(O) -> Unit,
        crossinline handle: suspend (S, I) -> O
): Pair<EndpointKey, NamedApiEndpointHandler<S>> {

    val handler: ApiEndpointHandler<S> = { sessionId ->
        val param = readParam()
        val response = handle(sessionId, param)
        val result: BytesReceiver.() -> Unit = { writeResponse(response) }
        result
    }

    return endpoint.key to NamedApiEndpointHandler(endpoint.name, handler)
}

inline fun <S> Map<EndpointKey, NamedApiEndpointHandler<S>>.collapse(
        crossinline onEndpointKeyDefined: (S, EndpointKey) -> Unit = { _, _ -> }
): ApiEndpointHandler<S> = { sessionId ->
    val key = read(EndpointKey.bytesAdapter.read)
    onEndpointKeyDefined(sessionId, key)
    val handler = this@collapse[key] ?: error("Unknown endpoint with key $key")
    handler.handle(this, sessionId)
}

fun <S> ApiEndpointHandler<S>.mapToResponseApiResponse(
        apiResponseReadWriteFactory: ApiResponseReadWriteFactory,
        unexpectedThrowableToApiError: (S, Throwable) -> ApiError
): ApiEndpointHandler<S> = { sessionId ->
    try {
        val responseWriter = this@mapToResponseApiResponse(sessionId)
        val result: (BytesReceiver).() -> Unit = {
            apiResponseReadWriteFactory.write(true, this, responseWriter)
        }
        result
    } catch (throwable: Throwable) {
        val error = throwable
                .castOrElse<ApiException> {
                    unexpectedThrowableToApiError(sessionId, throwable).exception
                }
                .error
        val result: (BytesReceiver).() -> Unit = {
            apiResponseReadWriteFactory.writeError(this, error)
        }
        result
    }
}